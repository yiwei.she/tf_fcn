# Define the assign operations so we can reuse pretrained 
# VGG weights from layers 1-7 (i.e. conv1-5, fc6, fc7)
# Note: must run the assign operations in the Session.

# Load pretrained weights, downloaded from 
# https://www.cs.toronto.edu/~frossard/vgg16/vgg16_weights.npz
from ext_imports import *

weights = np.load("data/vgg16_weights.npz")
weight_names = sorted(weights.keys())

def get_vgg_assign_ops(graph):
    
    assign_ops = []
    
    for weight in weight_names:
        # for each variable in the pretrained VGG network,
        # get the name of the corresponding tensor in our graph
        if weight[-1] == "W":
            var_name = "kernel"
        elif weight[-1] == "b":
            var_name = "bias"
        tensorname = "{}/{}:0".format(weight[:-2],var_name)
        
        # Use tf.assign to put pre-trained values into the
        # variables in our graph.
        # If the variable is for the fully connected layers,
        # handle differently so that FCN can process any sized input
        if weight[:3] == "fc6":
            if var_name == "kernel":
                var_tensor = np.reshape(weights[weight], (7,7,512,4096))
            elif var_name == "bias":
                var_tensor = weights[weight]
            assign_op = tf.assign(ref=graph.get_tensor_by_name(tensorname),value=var_tensor)
        elif weight[:3] == "fc7":
            if var_name == "kernel":
                var_tensor = np.reshape(weights[weight], (1,1,4096,4096))
            elif var_name == "bias":
                var_tensor = weights[weight]
            assign_op = tf.assign(ref=graph.get_tensor_by_name(tensorname),value=var_tensor)

        # Do not copy fc8 weights; we train this layer ourselves
        elif weight[:3] == "fc8":
            assign_op = None
        
        # Assign weights to conv layers.
        else:
            assign_op = tf.assign(ref=graph.get_tensor_by_name(tensorname),value=weights[weight])
            
        # Add the assign op to our list (unless fc8)
        if not assign_op is None:
            assign_ops.append(assign_op)
    return assign_ops