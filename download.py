from urllib.request import urlretrieve
import os
import subprocess

try:
    os.mkdir('data')
except:
  pass

os.chdir('data')
print(os.getcwd())

if "VOCtrainval_25-May-2011.tar" in os.listdir('.'):
    pass
else:
    subprocess.call(["wget http://host.robots.ox.ac.uk/pascal/VOC/voc2011/VOCtrainval_25-May-2011.tar"], shell=True)

if "VOCtrainval_25-May-2011" in os.listdir('.'):
    pass
else:
    print(os.getcwd())
    subprocess.Popen('tar -xvf VOCtrainval_25-May-2011.tar', shell=True)
